class Mapa
{
    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor()
    {
        this.posicionInicial=[4.607276, -74.153902];
        this.escalaInicial=25;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        this.atributosProveedor=
        {
            maxZoom:20
        }

        this.miVisor=L.map("mapId");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }
    colocarMarcador(posicion)
    {
      this.marcadores.push(L.marker(posicion));
      this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    coloccarCirculo(posicion,configuracion)
    {
      this.circulos.push(L.circle(posicion,configuracion));
      this.circulos[this.circulos.length-1].addTo(this.miVisor);
    }

    colocarPoligonos(posicion)
    {
        this.poligonos.push(L.polygon(posicion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }
}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.606758, -74.154803]);
miMapa.coloccarCirculo([4.606758, -74.154803]),
{
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 100
}
miMapa.colocarPoligonos([[4.606615, -74.155184],[4.607206, -74.154315],[4.606329, -74.154449]])